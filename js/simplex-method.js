function SimplexMethod(x_count, output_block_ID) {
    var xCount = parseInt(x_count);
    var outputBlock = $('#'+output_block_ID);

    this.fx = {};
    this.limits = {};

    function getFraction(number) {
        return typeof number !== 'object' ? math.fraction(math.number(number)) : number;
    }

    function formatFraction(fraction) {
        return fraction.d === 1 ? fraction.s * fraction.n : math.format(fraction, {'fraction': 'ratio'});
    }

    /**
     * Check if the matrix has a positive F(s) and returns the array positive of keys sorted by max value
     * @param matrix Object
     * @return {Array}
     */
    function getPositiveFs(matrix) {
        var keysArr = [], i;
        for(i = 1; i <= xCount; i++) {
            if(matrix['F'][i] > 0) {
                keysArr.push(i);
            }
        }
        keysArr.sort(function(a, b){
            return matrix['F'][b] - matrix['F'][a];
        });
        return keysArr;
    }

    /***
     *
     * @param matrix Object
     * @param fKey Int|String
     * @return {Array}
     */
    function getPositiveXs(matrix, fKey) {
        var xArr = [], key;
        for(key in matrix) {
            if(key !== 'F' && matrix[key][fKey] > 0){
                xArr.push(key);
            }
        }
        return xArr;
    }

    function outputIterationTable(fx, matrix, iteration) {
        var i, key, last = xCount + 1;
        var html = '<div class="table-responsive" data-iteration="'+iteration+'">';
        html += '<div class="alert alert-info" role="alert"><h3 class="text-center">Ітерація №'+iteration+'</h3></div>';
        html += '<table class="table table-responsive table-bordered table-hover table-striped">';
        html += '<thead><tr>';
        html += '<th>#</th>';
        for(i = 1; i <= xCount; i++) html += '<th>x<sub>'+i+'</sub></th>';
        html += '<th>Вільні члени</th>';
        html += '</tr></thead>';
        html += '<tbody>';
        for(key in matrix) {
            html += '<tr>';
            if(key === 'F') {
                html += '<th>f</th>';
                for(i = 1; i <= xCount; i++) html += '<td>'+formatFraction(matrix[key][i])+'</td>';
                html += '<td>'+formatFraction(matrix[key][last])+'</td>';
            } else {
                html += '<th>x<sub>'+key+'</sub></th>';
                for(i = 1; i <= xCount; i++) html += '<td>'+formatFraction(matrix[key][i])+'</td>';
                html += '<td>'+formatFraction(matrix[key][last])+'</td>';
                if(iteration === 1) {
                    html += '<td>'+formatFraction(fx[key])+'</td>';
                }
            }
            html += '</tr>';
        }
        if(iteration === 1) {
            html += '<tr>';
            html += '<td>&nbsp;</td>';
            for(i = 1; i <= xCount; i++) html += '<td>'+formatFraction(fx[i])+'</td>';
            html += '</tr>';
        }
        html += '</tbody>';
        html += '</table>';
        html += '</div>';
        outputBlock.append(html);
    }

    function outputMessage(message, message_class) {
        var messageClass = typeof message_class === 'undefined' ? 'alert-info' : message_class;
        outputBlock.append('<div class="alert '+messageClass+'" role="alert">'+message+'</div>');
    }

    function outputMinResult(fx, matrix) {
        outputMessage('Серед оцінок немає додатніх. Отримано оптимальний розв\'язок задачі.',' alert-success');
        outputResult(fx, matrix, 'min', '*');
    }

    function outputResult(fx, matrix, iteration_or_name, x_sup) {
        var i, xArr = [], last = xCount + 1, html = '';
        html += '<div class="x-result-'+iteration_or_name+'">x<sup>'+x_sup+'</sup>(';
        for(i = 1; i <= xCount; i++) xArr.push(matrix.hasOwnProperty(i) ? formatFraction(matrix[i][last]) : 0);
        html += xArr.join(', ') + ')</div>';
        html += '<div class="fx-result-'+iteration_or_name+'">f(x<sup>'+x_sup+'</sup>) = '+ formatFraction(matrix['F'][last]) +'</div>';
        outputBlock.append(html);
    }

    this.getXCount = function () {
        return xCount;
    };

    this.compute = function () {
        var matrix = {}, i, key, inKey, lastCount = xCount + 1;
        /* Bases */
        for (i = 1; i <= xCount; i++) {
            var count = 0, coefficient = null, baseKey = null;
            for (key in this.limits) {
                for (inKey in this.limits[key]) {
                    if (inKey === i + '') {
                        coefficient = this.limits[key][inKey];
                        baseKey = key;
                        count++;
                    }
                }
            }
            if (count === 1 && coefficient === 1) {
                matrix[i] = this.limits[baseKey];
            }
        }
        var fx = Object.assign({}, this.fx);
        /* Matrix */
        for (key in matrix) {
            for (i = 1; i <= xCount; i++) matrix[key][i] = getFraction(matrix[key].hasOwnProperty(i) ? matrix[key][i] : 0);
            matrix[key][lastCount] = getFraction(matrix[key].free);
            delete matrix[key].free;
        }
        /* FX */
        for (i = 1; i <= xCount; i++) fx[i] = getFraction(fx.hasOwnProperty(i) ? fx[i] : 0);
        /* F (marks) */
        matrix['F'] = {};
        for (i = 1; i <= xCount; i++) {
            matrix['F'][i] = getFraction(0);
            for (key in matrix) {
                if (key !== 'F') {
                    matrix['F'][i] = math.add(matrix['F'][i], math.multiply(matrix[key][i], fx[key]));
                }
            }
            matrix['F'][i] = math.subtract(matrix['F'][i], fx[i]);
        }
        matrix['F'][lastCount] = getFraction(0);
        for (key in matrix) {
            if (key !== 'F') {
                matrix['F'][lastCount] = math.add(matrix['F'][lastCount], math.multiply(matrix[key][lastCount], fx[key]));
            }
        }
        var iteration = 1;
        while (1) {
            outputIterationTable(fx, matrix, iteration);
            /* Check for positive marks (F) */
            var fPositives = getPositiveFs(matrix), firstPositiveXs;
            if (fPositives.length) {
                /* Check for X */
                for (i = 0; i < fPositives.length; i++) {
                    var xs = getPositiveXs(matrix, fPositives[i]);
                    if (!xs.length) {
                        outputMessage('<p>Було знайдено стовпчик над додатньою оцінкою, де немає жодного додатнього елементу.</p>' +
                            '<p>Задача не має розв\'язку (оптимального), так як функція цілі не обмежена на МПЗ знизу.</p>', ' alert-danger');
                        return;
                    }
                    if (i === 0) {
                        firstPositiveXs = xs;
                    }
                }
                /* Go to the next iteration... */
                outputResult(fx, matrix, iteration, '0');
                outputMessage('Серед оцінок є додатні, над кожною є хоча б один додатній елемент.');
                /* Compute min F/x */
                var outOfBase = firstPositiveXs[0], inBase = fPositives[0];
                for (i = 0; i < firstPositiveXs.length; i++) {
                    if (
                        formatFraction( math.compare(
                            math.divide(matrix[firstPositiveXs[i]][lastCount], matrix[firstPositiveXs[i]][inBase]),
                            math.divide(matrix[outOfBase][lastCount], matrix[outOfBase][inBase])
                        ) ) == -1) {
                        outOfBase = firstPositiveXs[i];
                    }
                }
                outputMessage('<p>x<sub>' + inBase + '</sub> вводиться в базис.</p>' +
                    '<p>x<sub>' + outOfBase + '</sub> виводиться з базису.</p>');
                /* Rename with new base */
                Object.defineProperty(matrix, inBase,
                    Object.getOwnPropertyDescriptor(matrix, outOfBase));
                delete matrix[outOfBase];
                /* Div on new base */
                var div = matrix[inBase][inBase];
                for (i = 1; i <= lastCount; i++) {
                    matrix[inBase][i] = math.divide(matrix[inBase][i], div);
                }
                for (key in matrix) {
                    if (key != inBase) {
                        var productCoefficient = math.multiply(getFraction(-1), matrix[key][inBase]);
                        for (i = 1; i <= lastCount; i++) {
                            matrix[key][i] = math.add(matrix[key][i], math.multiply(productCoefficient, matrix[inBase][i]));
                        }
                    }
                }
                iteration++;
            } else {
                /* Min has been found */
                outputMinResult(fx, matrix);
                return;
            }
        }
    }
}

$(function () {

    var simplexMethod;

    function getXSelect(current) {
        var string = '';
        string += '<select name="limit' + current + '">';
        for (var i = 1, n = simplexMethod.getXCount(); i <= n; i++) {
            string += '<option value="x' + i + '">' + i + '</option>';
        }
        string += '</select>';
        return string;
    }

    function parseValue(value) {
        if (value === '+' || value === '') {
            return 1;
        } else if (value === '-') {
            return -1;
        } else {
            return parseInt(value);
        }
    }

    $('#first-step-form').submit(function (e) {
        e.preventDefault();
        var $this = $(this);
        simplexMethod = new SimplexMethod(
            $this.find('input[name="simplex-x-count"]').val(),
            'third-step'
        );
        var $secondSection = $this.parents('section').hide(400).next().show(400);
        var $fxGroup = $secondSection.find('#fx-group');
        for (var i = 1, n = simplexMethod.getXCount(); i <= n; i++) {
            $fxGroup.append('<input type="text" class="x" name="fx' + i + '" /><span id="fx-x' + i + '">x<sub>' + i + '</sub></span>');
        }
        $fxGroup.append('<span>&nbsp;<span class="glyphicon glyphicon-arrow-right"></span>&nbsp;min</span>');
    });

    $('#limit-button').click(function (e) {
        var $this = $(this);
        var $section = $this.parent().find('section');
        var countAll = $section.children().length;
        var current = countAll + 1;
        $section.append('<div><input type="text" class="x" name="limit' + current + '"/><span>x</span><sub>' + getXSelect(current) + '</sub><button type="button" data-current="' + current + '" class="btn add-x-button">+</button>' + '<span> = </span><input type="text" name="limit' + current + 'Free"/>' + '</div>');
    });

    $(document.body).on('click', '.add-x-button', function (e) {
        var $this = $(this);
        var current = $this.attr('data-current');
        $('<input type="text" class="x" name="limit' + current + '"/><span>x</span><sub>' + getXSelect(current) + '</sub>').insertBefore($this);
    });

    $('#second-step-form').submit(function (e) {
        e.preventDefault();
        var $this = $(this);
        var rawData = $this.serializeArray();
        var fx = {};
        var limits = {};
        for (var i = 0; i < rawData.length; i++) {
            var value = rawData[i];
            if (value.name.indexOf('fx') === 0) {
                fx[value.name.replace('fx', '')] = parseValue(value.value);
            }
            if (value.name.indexOf('limit') === 0) {
                var j = value.name.replace('limit', '');
                limits[j] = {};
                while (rawData[i].name.indexOf('Free') === -1) {
                    var value2 = 0;
                    if (rawData[i].value.indexOf('/') === -1) {
                        value2 = parseValue(rawData[i].value);
                    } else {
                        var values2 = rawData[i].value.split('/');
                        value2 = math.fraction(parseValue(values2[0]), parseValue(values2[1]));
                    }
                    limits[j][rawData[i + 1].value.replace('x', '')] = value2;
                    i += 2;
                }
                var valueFree = 0;
                if (rawData[i].value.indexOf('/') === -1) {
                    valueFree = parseValue(rawData[i].value);
                } else {
                    var valuesFree = rawData[i].value.split('/');
                    valueFree = math.fraction(parseValue(valuesFree[0]), parseValue(valuesFree[1]));
                }
                limits[j]['free'] = valueFree;
            }
        }
        simplexMethod.fx = fx;
        simplexMethod.limits = limits;
        simplexMethod.compute();
        $this.parents('section').hide(400).next().show(400);
    });
});
